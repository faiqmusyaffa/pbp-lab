from django.shortcuts import render, redirect
from lab_2.models import Note
from .forms import NoteForm 
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    notes = Note.objects.all()
    response = {'notes' : notes}
    return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def add_note(request):
    form = NoteForm(request.POST or None)
    if form.is_valid():
        form.save()
    context = {'form' : form}

    # Redirect to lab-4
    if request.method == "POST":
        return redirect("/lab-4")

    return render(request, 'lab4_form.html', context)

@login_required(login_url='/admin/login/')
def note_list(request):
    notes = Note.objects.all()
    response = {'notes' : notes}
    return render(request, 'lab4_note_list.html', response)